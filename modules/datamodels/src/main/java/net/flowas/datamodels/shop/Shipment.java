package net.flowas.datamodels.shop;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
public class Shipment extends IdAndDate {

    //@Column(name = "reserved_order")
    @ManyToOne
    private ShoppingCart order;
    @Column(name = "NAME")
    private String name;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "DELIVERY_METHOD_ID")
    private Long deliveryMethodId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EXPECTED_DELIVERY_DATE")
    private Date expectedDeliveryDate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    //@JoinColumn(name="shipment")
    private Set<OrderItem> orderItems = new HashSet<OrderItem>();
}
