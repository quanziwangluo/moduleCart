/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import net.flowas.datamodels.shop.enumtype.ProductAssociationLinkType;

/**
 *
 * @author liujh
 */
@Data
@Entity
public class ProductAssociationLink  extends IdAndDate{
    private int rankPosition;
    @ManyToOne
    private Product product;
    @Enumerated(EnumType.STRING) 
    private ProductAssociationLinkType type;
    
    @Temporal(TemporalType.TIMESTAMP)	
    private Date dateStart;
	
    @Temporal(TemporalType.TIMESTAMP)	
    private Date dateEnd;
}
