/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.Data;

/**
 *
 * @author liujh
 */
@Data
@Entity
public class ProductAttribute extends IdAndDate{
    private String textValue;
    @ManyToOne
    private AttributeDefinition attributeDefinition;
}
