-- CREATE OR REPLACE FUNCTION select_proc2(int) RETURNS SETOF unknown AS $func$ SELECT 'name' FROM ss_group where id = $1; $func$ LANGUAGE sql

INSERT INTO ss_group(id, code, description, name, sortorder, parent_id) VALUES (1, 'finacial', 'finacial', '财务部', 1, null);
INSERT INTO ss_group(id, code, description, name, sortorder, parent_id) VALUES (2, 'it', 'it', 'IT部', 2, null);
INSERT INTO ss_group(id, code, description, name, sortorder, parent_id) VALUES (3, 'soft', 'soft', '软件', 3, null);
INSERT INTO ss_group(id, code, description, name, sortorder, parent_id) VALUES (4, 'web', 'web', '前端开发', 4, 1);
INSERT INTO ss_group(id, code, description, name, sortorder, parent_id) VALUES (5, 'distr', 'distr', '分布式', 5, 1);
INSERT INTO ss_user(id, email, loginname, name, password, salt, status, group_id) VALUES (1, 'flowas@gmail.com', 'flowas', 'flowas', 'flowas', 'salt', 'normal', 1);
INSERT INTO material(id, name, position_no, price, shortname, unit) VALUES (1, 'test', 'code', 12, 'test', '公里');
